#!/bin/bash

# Dmenu script for editing some of my more frequently edited config files.


declare options=("logout
reboot
shutdown
quit")

choice=$(echo -e "${options[@]}" | dmenu -i -p "Are You Sure? You Want To Quit: ")

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	logout)
	    choice="killall awesome"
	;;
	reboot)
		choice="reboot"
	;;
	shutdown)
		choice="shutdown"
	;;
	*)
		exit 1
	;;
esac
alacritty -e $choice
