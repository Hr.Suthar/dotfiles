alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias l.='ls -A | egrep "^\."'

alias ..='cd ..' 
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

alias xed='/usr/local/bin/devour xed'

alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"

